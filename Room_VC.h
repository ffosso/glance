//
//  Room_VC.h
//  Glance
//
//  Created by FFabio on 07/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#define SHOWED 1
#define NOT_SHOWED 0
#define IDPOST_NULL -1
#define TIMER_CHECK_CONNECTION 50000
#define TIMER_UPDATE_DATA 100000
#define SEGUE_ROOMLIST @"roomList"
#define SEGUE_SENDPOST @"sendPost"
#define SEGUE_COMMENT_ACTION @"CommentAction"
#define SEGUE_DETAILS_ACTION @"DetailsAction"
#define SEGUE_COMMENT_ACTION1 @"CommentAction1"
#define SEGUE_DETAILS_ACTION1 @"DetailsAction1"

#import <UIKit/UIKit.h>
#import "AFImageRequestOperation.h"
#import "Settings.h"
#import "PostCell.h"
#import "PostCellWithPhotoCell.h"
#import "SendPost_VC.h"
#import "PostDetails_VC.h"

@interface Room_VC : UIViewController <UITableViewDataSource, UITableViewDelegate, WebApiManagerDelegate>
{
    @private
    AppDelegate *appDelegate;
    WebApiManager *waManager;
    DataCenter *dataCenter;
    ManageLogin *mLogin;
    User *user;
    
    UITableView *tablePosts;
    Spot *spot;
    Post *postSelected;
    
    NSCache *cachePostPhotos;
    NSCache *cachePostUserPhotos;
    
    NSTimer *checkConnectionTimer;
    NSTimer *reloadDataTimer;
    CheckConnection *checkConnection;
    BOOL connectionStatusChanged;
//    BOOL reloadByTimer;
}

@property(nonatomic, strong) IBOutlet UILabel *spotName;
@property(nonatomic, strong) IBOutlet UITableView *tablePosts;
@property(nonatomic, strong) IBOutlet UIView *alertBannerView;
@property(nonatomic, strong) IBOutlet UIView *errorConnectionView;
@property(nonatomic, strong) IBOutlet UIView *updateView;
@property(nonatomic, strong) IBOutlet UILabel *numberOfUpdates;
@property(nonatomic, strong) IBOutlet UILabel *labelUpdates;

- (void) reloadDataBySpot:(Spot *)s andLastPostID:(NSString *)lastPostID;

- (IBAction) sendILike:(id)sender;
- (IBAction) scrollToTop:(id)sender;

@end
