//
//  CommentCell.m
//  Glance
//
//  Created by FFabio on 18/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

@synthesize userPhoto = _userPhoto, comment = _comment;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
