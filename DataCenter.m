//
//  DataCenter.m
//  Glance
//
//  Created by FFabio on 21/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "DataCenter.h"

@implementation DataCenter

@synthesize spotListTipology = _spotListTipology, spotList = _spotList, postList = _postList, locations = _locations, commentList = _commentList;

- (id) init
{
    self = [super init];
    if (self) {
        self.spotListTipology = Nil;
        self.spotList = Nil;
        self.commentList = Nil;
    }
    return self;
}

#pragma mark - GET Methods

- (NSMutableDictionary *) getSpotListDividedInTipology
{
    NSMutableDictionary *spots = [[NSMutableDictionary alloc] init];
    for (int i=0; i<self.spotListTipology.count; i++) {
        NSMutableArray *spotByTipology = [[NSMutableArray alloc] init];
        for (int y=0; y<self.spotList.count; y++) {
            if ([[ (Spot *) [self.spotList objectAtIndex:y] tipology] isEqualToString:[ (SpotTipology *) [self.spotListTipology objectAtIndex:i] name]])
                [spotByTipology addObject:(Spot *) [self.spotList objectAtIndex:y]];
        }
        [spots setObject:spotByTipology forKey:[ (SpotTipology *) [self.spotListTipology objectAtIndex:i] name]];
    }
    return spots;
}

@end
