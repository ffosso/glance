//
//  CheckConnection.h
//  Glance
//
//  Created by FFabio on 27/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface CheckConnection : NSObject
{
    Reachability *internetReachable;
    BOOL alertIsShowed;
}

- (BOOL) checkConnectionAndShowBanner:(UIView *)alertBannerView;
- (BOOL) checkConnectionAndShowConnectionError:(UIView *)view;

@end
