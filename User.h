//
//  User.h
//  Glance
//
//  Created by FFabio on 29/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property(strong, nonatomic) NSString *userFacebookID;
@property(strong, nonatomic) NSString *userFacebookName;
@property(strong, nonatomic) NSString *userFacebookPhotoLink;
@property(strong, nonatomic) UIImage *userFacebookPhoto;

- (id) initWithFBID:(NSString *)userFBID FBName:(NSString *)userFBName FBPhotoLink:(NSString *)userFBPhotoLink;

@end
