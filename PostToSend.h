//
//  PostToSend.h
//  Glance
//
//  Created by FFabio on 08/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostToSend : NSObject

@property(strong, nonatomic) NSString *spotID;
@property(strong, nonatomic) NSString *userID;
@property(strong, nonatomic) NSString *body;
@property(strong, nonatomic) NSString *anonimousMode;
@property(strong, nonatomic) UIImage *postPhoto;

- (id)initWithID:(NSString *)idSpot userID:(NSString *)userID body:(NSString *)body anonimous:(NSString *)anonimousMode postPhoto:(UIImage *) postPhoto;

@end
