//
//  PostToSend.m
//  Glance
//
//  Created by FFabio on 08/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "PostToSend.h"

@implementation PostToSend

@synthesize spotID = _spotID, userID = _userID, body = _body, anonimousMode = _anonimousMode, postPhoto = _postPhoto;

- (id)initWithID:(NSString *)idSpot userID:(NSString *)userID body:(NSString *)body anonimous:(NSString *)anonimousMode postPhoto:(UIImage *) postPhoto
{
    self = [super init];
    if (self) {
        _spotID = idSpot;
        _userID = userID;
        _body = body;
        _anonimousMode = anonimousMode;
        _postPhoto = postPhoto;
    }
    return self;
}

@end
