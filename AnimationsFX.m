
//
//  AnimationsFX.m
//  Glance
//
//  Created by FFabio on 27/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "AnimationsFX.h"

@implementation AnimationsFX

#pragma mark - UIView Animations

+ (void) showViewByTraslation:(UIView *)viewToAnimate toShow:(BOOL)show
{
    if (show)
        [UIView animateWithDuration:TRANSLATE_ALERT_DURATION delay:0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void) {
                         viewToAnimate.transform = CGAffineTransformTranslate(viewToAnimate.transform, 0, -viewToAnimate.frame.size.height);
                     } completion:^(BOOL finished){ }
         ];
    else
        [UIView animateWithDuration:TRANSLATE_ALERT_DURATION delay:0 options:UIViewAnimationOptionCurveEaseInOut
                         animations:^(void) {
                             viewToAnimate.transform = CGAffineTransformTranslate(viewToAnimate.transform, 0, +viewToAnimate.frame.size.height);
                         } completion:^(BOOL finished){ }
         ];
}

+ (void) showViewByTraslation:(UIView *)viewToAnimate traslationX:(double)xTraslation traslationY:(double)yTraslation duration:(double)duration
{
    [UIView animateWithDuration:duration animations:^{
        viewToAnimate.frame = CGRectMake(xTraslation, yTraslation, viewToAnimate.frame.size.width, viewToAnimate.frame.size.height);
    }];
}

+ (void) showConnectionErrorView:(UIView *)viewToAnimate toShow:(BOOL)show
{
    viewToAnimate.hidden = !show;
}

+ (void) showAlertDialogWithTitle:(NSString *)title message:(NSString *)message firstButtonTitle:(NSString *)msgFirstButton secondButtonTitle:(NSString *)msgSecondButton
{
    UIAlertView *alert;
    if (msgSecondButton)
        alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:msgFirstButton otherButtonTitles:msgSecondButton,nil];
    else
        alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:msgFirstButton otherButtonTitles:nil];
    [alert show];
}

#pragma mark - UILabel Dynamics

+ (CGRect) calculateHeightOfUIlabelByText:(NSString *)text andLabel:(UILabel *)label
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width,FLT_MAX);
    
    CGSize expectedLabelSize = [text sizeWithFont:label.font
                                 constrainedToSize:maximumLabelSize
                                     lineBreakMode:label.lineBreakMode];
    
    CGRect newFrame = label.frame;
    newFrame.size.height = expectedLabelSize.height;
    return newFrame;
}

+ (int) calculateNumberLinesOfUIlabelByText:(NSString *)text andLabel:(UILabel *)label
{
    return [text sizeWithFont:label.font constrainedToSize:label.frame.size lineBreakMode:UILineBreakModeWordWrap].height / label.font.pointSize;
}

@end
