//
//  SpotTipology.h
//  Glance
//
//  Created by FFabio on 21/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpotTipology : NSObject

@property(readonly) NSUInteger idLocationTipology;
@property(strong, nonatomic, readonly) NSString *name;
@property(readonly) NSUInteger number;

- (id)initWithID:(NSUInteger)idLocationTipology name:(NSString *)name number:(NSUInteger)number;

@end
