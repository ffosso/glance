//
//  PostCell.m
//  Glance
//
//  Created by FFabio on 07/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "PostCell.h"

@implementation PostCell

@synthesize userPhoto = _userPhoto, userName = _userName, postTime = _postTime, numberLikeAndComment = _numberLikeAndComment, postBody = _postBody, iLikeButton = _iLikeButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
