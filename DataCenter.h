//
//  DataCenter.h
//  Glance
//
//  Created by FFabio on 21/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataCenter : NSObject

@property(strong, nonatomic) NSArray *spotListTipology;
@property(strong, nonatomic) NSArray *spotList;
@property(strong, nonatomic) NSArray *postList;
@property(strong, nonatomic) NSArray *locations;
@property(strong, nonatomic) NSArray *commentList;

- (id) init;
- (NSMutableDictionary *) getSpotListDividedInTipology;

@end
