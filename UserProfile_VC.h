//
//  UserProfile_VC.h
//  Glance
//
//  Created by FFabio on 15/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UserProfile_VC : UIViewController

@property(strong, nonatomic) IBOutlet UIButton *facebookLoginBtn;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property(nonatomic, strong) IBOutlet UIView *errorConnectionView;

- (IBAction) makeLogin:(id)sender;

@end
