//
//  Post.m
//  Glance
//
//  Created by FFabio on 21/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "Post.h"

@implementation Post

@synthesize idPost = _idPost, userName = _userName, userSurname = _userSurname, facebookUserID = _facebookUserID, body = _body, numberLike = _numberLike, numberComment = _numberComment, timeStamp = _timeStamp, userPhotoURL = _userPhotoURL, postPhotoURL = _postPhotoURL, iLikeEnabled = _iLikeEnabled;

- (id)initWithID:(NSUInteger)idPost userName:(NSString *)userName userSurame:(NSString *)userSurame facebookUserID:(NSString *)facebookUserID body:(NSString *)body numberLike:(NSString *)numberLike numberComment:(NSString *)numberComment timeStamp:(NSString *)timeStamp userPhotoURL:(NSString *)userPhotoURL postPhotoURL:(NSString *)postPhotoURL iLikeEnabled:(NSString *)iLikeEnabled;
{
    self = [super init];
    if (self) {
        _idPost = idPost;
        _userName = userName;
        _userSurname = userSurame;
        _facebookUserID = facebookUserID;
        _body = body;
        _numberLike = numberLike;
        _numberComment = numberComment;
        _timeStamp = timeStamp;
        _userPhotoURL = userPhotoURL;
        _postPhotoURL = postPhotoURL;
        _iLikeEnabled = iLikeEnabled;
    }
    return self;
}

@end
