//
//  Room_VC.m
//  Glance
//
//  Created by FFabio on 07/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "Room_VC.h"

@interface Room_VC ()

@end

@implementation Room_VC

@synthesize spotName, tablePosts, alertBannerView, errorConnectionView, updateView, numberOfUpdates, labelUpdates;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    reloadDataTimer = FALSE;
    checkConnectionTimer = [NSTimer scheduledTimerWithTimeInterval:TIMER_CHECK_CONNECTION target:self selector:@selector(checkConnection) userInfo:nil repeats:TRUE];
    reloadDataTimer = [NSTimer scheduledTimerWithTimeInterval:TIMER_UPDATE_DATA target:self selector:@selector(reloadDataByTimer) userInfo:nil repeats:TRUE];
    checkConnection = [[CheckConnection alloc] init];
    
    dataCenter = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getDataCenter];
    
//    waManager = [[WebApiManager alloc] init];
    //    [waManager setDelegate:self];
    waManager = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getWebApiManager];
    [waManager addDelegate:self];
    
    mLogin = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getLoginManager];
//    user = [mLogin getUserInfo];
    
    cachePostPhotos = [[NSCache alloc] init];
    cachePostUserPhotos = [[NSCache alloc] init];
    [self reloadDataByGPS ];
	// Do any additional setup after loading the view.
}

- (void) viewDidUnload
{
    [super viewDidUnload];
    [waManager removeDelegate:self];
    [checkConnectionTimer invalidate];
    checkConnectionTimer = Nil;
    [reloadDataTimer invalidate];
    reloadDataTimer = Nil;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    connectionStatusChanged = [checkConnection checkConnectionAndShowConnectionError:errorConnectionView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Delegate methods

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:SEGUE_SENDPOST ])
        [(SendPost_VC*) [segue destinationViewController] loadSpotData:spot];
    else if ([segue.identifier isEqualToString:SEGUE_ROOMLIST ]);
    else {
        if ([segue.identifier isEqualToString:SEGUE_COMMENT_ACTION ] || [segue.identifier isEqualToString:SEGUE_COMMENT_ACTION1 ])
                [(PostDetails_VC*) [segue destinationViewController] openKeyboard:TRUE];
        else if ([segue.identifier isEqualToString:SEGUE_DETAILS_ACTION ] || [segue.identifier isEqualToString:SEGUE_DETAILS_ACTION1 ])
                [(PostDetails_VC*) [segue destinationViewController] openKeyboard:FALSE];
        [(PostDetails_VC*) [segue destinationViewController] setPostDetails:postSelected andSpot:spot];
    }
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataCenter.postList.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    Post *post = [dataCenter.postList objectAtIndex:indexPath.row];
    if (![post.postPhotoURL isEqual:[NSNull null]]) {
        PostCellWithPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_PHOTO_POST_ID];
        
        cell.iLikeButton.enabled = TRUE;
        if (![post.iLikeEnabled isEqual:[NSNull null]])
            cell.iLikeButton.enabled = FALSE;
        
        cell.postPhoto.image = [UIImage imageNamed:@"postPhotoPlaceHolder.png"];
        cell.userPhoto.image = [UIImage imageNamed:@"defaultAnonymous.png"];
        
        cell.userName.text = ([post.userName isEqual:[NSNull null]] && [post.userSurname isEqual:[NSNull null]]) ? @"Anonimo" : [NSString stringWithFormat:@"%@ %@", post.userName, post.userSurname];
        
        cell.postBody.text = post.body;
        cell.postTime.text = post.timeStamp;
        
        NSString *nLikes = ([post.numberLike isEqual:[NSNull null]]) ? @"0" : post.numberLike;
        NSString *nComments = ([post.numberComment isEqual:[NSNull null]]) ? @"0" : post.numberComment;
        if ([nComments isEqualToString:@"1"])
            cell.numberLikeAndComment.text = [NSString stringWithFormat:@"%@ Mi piace - %@ Commento", nLikes, nComments];
        else
            cell.numberLikeAndComment.text = [NSString stringWithFormat:@"%@ Mi piace - %@ Commenti", nLikes, nComments];
        
        if (![post.userPhotoURL isEqual:[NSNull null]])
            [self downloadUserPhotoAsyncByIndexPath:indexPath];
        [self downloadImagesAsyncByIndexPath:indexPath];
        
        
        return cell;
    }
    
    PostCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_POST_ID];
    
    cell.iLikeButton.enabled = TRUE;
    if (![post.iLikeEnabled isEqual:[NSNull null]])
        cell.iLikeButton.enabled = FALSE;
    
    cell.userPhoto.image = [UIImage imageNamed:@"defaultAnonymous.png"];
    
    cell.userName.text = ([post.userName isEqual:[NSNull null]] && [post.userSurname isEqual:[NSNull null]]) ? @"Anonimo" : [NSString stringWithFormat:@"%@ %@", post.userName, post.userSurname];
    
    cell.postBody.text = post.body;
    cell.postTime.text = post.timeStamp;

    NSString *nLikes = ([post.numberLike isEqual:[NSNull null]]) ? @"0" : post.numberLike;
    NSString *nComments = ([post.numberComment isEqual:[NSNull null]]) ? @"0" : post.numberComment;
    if ([nComments isEqualToString:@"1"])
        cell.numberLikeAndComment.text = [NSString stringWithFormat:@"%@ Mi piace - %@ Commento", nLikes, nComments];
    else
        cell.numberLikeAndComment.text = [NSString stringWithFormat:@"%@ Mi piace - %@ Commenti", nLikes, nComments];

    if (![post.userPhotoURL isEqual:[NSNull null]])
        [self downloadUserPhotoAsyncByIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Post *post = [dataCenter.postList objectAtIndex:indexPath.row];
    NSLog(@"heightForRowAtIndexPath - indexPath: %d - %@", indexPath.row, post.postPhotoURL);
    if (![post.postPhotoURL isEqual:[NSNull null]])
        return POSTCELLWITHPHOTO_HEIGHT;
    return POSTCELL_HEIGHT;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    postSelected = [dataCenter.postList objectAtIndex:indexPath.row];
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self reloadCellImages];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self reloadCellImages];
}

/*- (void ) scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y == 0)
        [tablePosts reloadData];
}*/

- (void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y == 0)
        [self reloadCellImages];
}

- (void) sendPostCompletion:(NSError *)error
{
    [self reloadDataBySpot:spot andLastPostID:Nil];
}

#pragma mark - IBAction Methods

- (IBAction) sendILike:(id)sender
{
    if (!user) {
        NSThread *loginThread = [[NSThread alloc] init];
        [loginThread start];
        [self performSelector:@selector(makeLogin) onThread:loginThread withObject:Nil waitUntilDone:NO];
    }
//    [(UIButton *) sender setEnabled:FALSE];
    UITableViewCell* cell = (UITableViewCell *) [[sender superview] superview];
    NSIndexPath *indexPath = [tablePosts indexPathForCell:cell];
    Post *post = [dataCenter.postList objectAtIndex:indexPath.row];
    /*NSString *response = */[waManager sendILikeWithUserID:USER_ID_TEST postID:[NSString stringWithFormat:@"%d", post.idPost] commentID:Nil];
    dataCenter.postList = [waManager getPostBySpot:[NSString stringWithFormat:@"%d", spot.idSpot]];
    [tablePosts reloadData];
    [self reloadCellImages];
}

- (void) makeLogin
{
    user = [mLogin getUserInfo];
}

- (IBAction) scrollToTop:(id)sender
{
    if (updateView.tag == SHOWED) {
        updateView.tag = NOT_SHOWED;
        [AnimationsFX showViewByTraslation:updateView toShow:FALSE];
        [tablePosts reloadData];
        [tablePosts scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
        if (tablePosts.contentOffset.y == 0)
            [self reloadCellImages];
    }
}

#pragma mark - Inner Methods

- (void) reloadCellImages
{
    NSArray *visiblePath = [tablePosts indexPathsForVisibleRows];
    for (NSIndexPath *indexPath in visiblePath) {
        [self downloadImagesAsyncByIndexPath:indexPath];
        [self downloadUserPhotoAsyncByIndexPath:indexPath];
    }
}

- (void) checkConnection
{
/*    BOOL b = */[checkConnection checkConnectionAndShowConnectionError:errorConnectionView];
/*    if (connectionStatusChanged != b ) {
        connectionStatusChanged = b;
        [self reloadDataBySpot:spot];
    }*/
}

- (void) reloadDataByTimer
{
    if (dataCenter.postList.count != 0) {
        [self reloadDataBySpot:spot andLastPostID:[NSString stringWithFormat:@"%d", [(Post *) [dataCenter.postList objectAtIndex:0] idPost]]];
    }
    else
        [self reloadDataBySpot:spot andLastPostID:[NSString stringWithFormat:@"%d", IDPOST_NULL]];
}

- (void) reloadManually
{
    [self reloadDataBySpot:spot andLastPostID:Nil];
}

- (void) reloadDataBySpot:(Spot *)s andLastPostID:(NSString *)lastPostID
{
    spot = s;
    if (s.name)
        spotName.text = s.name;
    if (!lastPostID) {
        dataCenter.postList = [waManager getPostBySpot:[NSString stringWithFormat:@"%d", s.idSpot]];
        [tablePosts reloadData];
        [tablePosts scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:FALSE];
        [self reloadCellImages];
    }
    else {
        //Ci si entra solo dall' NSTimer che gestisce le update temporizzate
        NSDictionary *dict = [waManager getPostBySpot:[NSString stringWithFormat:@"%d", s.idSpot] andLastPostId:lastPostID];
//        NSLog(@"nuovi aggiornamenti: %d", [[dict objectForKey:@"NumeroNuoviPost"] intValue]);
        if ([[dict objectForKey:@"NumeroNuoviPost"] intValue] > 0) {
            labelUpdates.text = ([[dict objectForKey:@"NumeroNuoviPost"] intValue] == 1) ? @"nuovo aggiornamento" : @"nuovi aggiornamenti";
            if (updateView.tag == NOT_SHOWED) {
                updateView.tag = SHOWED;
                numberOfUpdates.text = ([[dict objectForKey:@"NumeroNuoviPost"] intValue] < 100) ? [dict objectForKey:@"NumeroNuoviPost"] : @"100+";
                [AnimationsFX showViewByTraslation:updateView toShow:TRUE];
            }
            dataCenter.postList = [dict objectForKey:@"PostList"];
        }
    }
    reloadDataTimer = FALSE;
}

- (void) reloadDataByGPS {
    
    NSString *latitudeString = [NSString stringWithFormat:@"%.6f", ((CLLocation *) [dataCenter locations].lastObject).coordinate.latitude];
    NSString *longitudeString = [NSString stringWithFormat:@"%.6f", ((CLLocation *) [dataCenter locations].lastObject).coordinate.longitude];
    
    NSDictionary *dict = [waManager getPostByGPSWithLatitude:latitudeString andLongitude:longitudeString];
    dataCenter.postList = [dict objectForKey:@"PostList"];
    spotName.text = [dict objectForKey:@"NomeLuogo"];
    spot = [[Spot alloc] initWithID:[[dict objectForKey:@"idLuogo"] intValue]
                               name:[dict objectForKey:@"NomeLuogo"]
                               city:Nil
                           tipology:Nil
                           latitude:latitudeString
                          longitude:longitudeString
                               date:Nil];
    if (dataCenter.postList.count != 0) {
        [tablePosts reloadData];
        [tablePosts scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:FALSE];
    }
}

- (void) downloadImagesAsyncByIndexPath:(NSIndexPath *)indexPath
{
    Post *post = [dataCenter.postList objectAtIndex:indexPath.row];
    if (![post.postPhotoURL isEqual:[NSNull null]]) {
        __block NSData *imageData = [cachePostPhotos objectForKey:post.postPhotoURL];
        if (imageData) {
            [(PostCellWithPhotoCell *) [tablePosts cellForRowAtIndexPath:indexPath] postPhoto].image = [UIImage imageWithData:imageData];
        } else {
            dispatch_queue_t imageQueue_ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
            dispatch_async(imageQueue_, ^{
                if (tablePosts.dragging == FALSE && tablePosts.decelerating == FALSE) {
                    imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:post.postPhotoURL]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (imageData) {
                            [cachePostPhotos setObject:imageData forKey:post.postPhotoURL];
                            [(PostCellWithPhotoCell *) [tablePosts cellForRowAtIndexPath:indexPath] postPhoto].image = [UIImage imageWithData:imageData];
                        }
                    });
                }
            });
        }
    }
}

- (void) downloadUserPhotoAsyncByIndexPath:(NSIndexPath *) indexPath
{
    Post *post = [dataCenter.postList objectAtIndex:indexPath.row];
    if (![post.userPhotoURL isEqual:[NSNull null]]) {
        __block NSData *userPhotoData = [cachePostUserPhotos objectForKey:post.userPhotoURL];
        if (userPhotoData) {
            if (![post.postPhotoURL isEqual:[NSNull null]])
                [(PostCellWithPhotoCell *) [tablePosts cellForRowAtIndexPath:indexPath] userPhoto].image = [UIImage imageWithData:userPhotoData];
            else
                [(PostCell *) [tablePosts cellForRowAtIndexPath:indexPath] userPhoto].image = [UIImage imageWithData:userPhotoData];
        } else {
            dispatch_queue_t imageQueue_ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
            dispatch_async(imageQueue_, ^{
                if (tablePosts.dragging == FALSE && tablePosts.decelerating == FALSE) {
                    userPhotoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:post.userPhotoURL]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cachePostUserPhotos setObject:userPhotoData forKey:post.userPhotoURL];
                        if (![post.postPhotoURL isEqual:[NSNull null]]) {
                            if (userPhotoData)
                                [(PostCellWithPhotoCell *) [tablePosts cellForRowAtIndexPath:indexPath] userPhoto].image = [UIImage imageWithData:userPhotoData];
                        }
                        else {
                            if (userPhotoData)
                                [(PostCell *) [tablePosts cellForRowAtIndexPath:indexPath] userPhoto].image = [UIImage imageWithData:userPhotoData];
                        }
                    });
                }
            });
        }
    }
}

@end
