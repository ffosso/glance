//
//  AnimationsFX.h
//  Glance
//
//  Created by FFabio on 27/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#define TRANSLATE_ALERT_DURATION 0.5

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface AnimationsFX : NSObject

+ (void) showViewByTraslation:(UIView *)viewToAnimate toShow:(BOOL)show;
+ (void) showViewByTraslation:(UIView *)viewToAnimate traslationX:(double)xTraslation traslationY:(double)yTraslation duration:(double)duration;
+ (void) showConnectionErrorView:(UIView *)viewToAnimate toShow:(BOOL)show;
+ (void) showAlertDialogWithTitle:(NSString *)title message:(NSString *)message firstButtonTitle:(NSString *)msgFirstButton secondButtonTitle:(NSString *)msgSecondButton;
+ (CGRect) calculateHeightOfUIlabelByText:(NSString *)text andLabel:(UILabel *)label;
+ (int) calculateNumberLinesOfUIlabelByText:(NSString *)text andLabel:(UILabel *)label;

@end
