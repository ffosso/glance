//
//  Settings.h
//  Glance
//
//  Created by FFabio on 07/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject

#pragma mark - Room_VC

#define CELL_POST_ID @"postCell"
#define CELL_PHOTO_POST_ID @"postCellPhoto"
#define CELL_MENU_ID @"menuCell"
#define SERVER_URL @"http://www.beeshout.com/"
#define WWW_SERVER_URL @"www.beeshout.com"
#define SPOT_LIST_APIURL @"Glance/getLuoghi.php"
#define SPOT_TIPOLOGY_LIST_APIURL @"Glance/getLuogoType.php"
#define POST_BY_SPOTID_APIURL @"Glance/getPost.php"
#define COMMENTS_BY_POSTID @"Glance/getCommenti.php"
#define I_LIKE_APIURL @"Glance/postMipiace.php"
#define SEND_POST_URL @"Glance/postPost.php"
#define PHOTO_EXTENTION @".jpg"

#define POSTCELL_HEIGHT 185.0
#define POSTCELLWITHPHOTO_HEIGHT 466.0
#define COMMENTCELL_HEIGHT 70.0

#define USER_ID_TEST @"2"

@end
