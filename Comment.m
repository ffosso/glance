//
//  Comment.m
//  Glance
//
//  Created by FFabio on 17/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "Comment.h"

@implementation Comment

@synthesize commentID = _commentID, body = _body, timestamp = _timestamp, userID = _userID, postID = _postID, userPhotoURL = _userPhotoURL;

- (id) initWithCommentID:(NSString *)commentID body:(NSString *)body timestamp:(NSString *)timestamp userID:(NSString *)userID postID:(NSString *)postID userPhotoURL:(NSString *)userPhotoURL
{
    self = [super init];
    if (self) {
        _commentID = commentID;
        _body = body;
        _timestamp = timestamp;
        _userID = userID;
        _postID = postID;
        _userPhotoURL = userPhotoURL;
    }
    return self;
}

@end
