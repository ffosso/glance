//
//  SpotTipology.m
//  Glance
//
//  Created by FFabio on 21/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "SpotTipology.h"

@implementation SpotTipology

@synthesize idLocationTipology = _idLocationTipology, name = _name, number = _number;

- (id)initWithID:(NSUInteger)idLocationTipology name:(NSString *)name number:(NSUInteger)number;
{
    self = [super init];
    if (self) {
        _idLocationTipology = idLocationTipology;
        _name = name;
        _number = number;
    }
    return self;
}

@end
