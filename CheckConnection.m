//
//  CheckConnection.m
//  Glance
//
//  Created by FFabio on 27/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "CheckConnection.h"

@implementation CheckConnection

- (id) init
{
    self = [super init];
    if (self) {
        alertIsShowed = FALSE;
    }
    return self;
}

- (BOOL) checkConnectionAndShowBanner:(UIView *)alertBannerView
{
    internetReachable = [Reachability reachabilityWithHostname:WWW_SERVER_URL];
    
    // Internet is reachable
    internetReachable.reachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            if (alertIsShowed) {
                alertIsShowed = FALSE;
                [AnimationsFX showViewByTraslation:alertBannerView toShow:FALSE];
            }
        });
    };
    
    // Internet is not reachable
    internetReachable.unreachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!alertIsShowed) {
                alertIsShowed = TRUE;
                [AnimationsFX showViewByTraslation:alertBannerView toShow:TRUE];
            }
        });
    };
    
    [internetReachable startNotifier];
    return !alertIsShowed;
}


- (BOOL) checkConnectionAndShowConnectionError:(UIView *)view
{
    internetReachable = [Reachability reachabilityWithHostname:WWW_SERVER_URL];
    
    // Internet is reachable
    internetReachable.reachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            if (alertIsShowed) {
                alertIsShowed = FALSE;
                [AnimationsFX showConnectionErrorView:view toShow:FALSE];
            }
        });
    };
    
    // Internet is not reachable
    internetReachable.unreachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!alertIsShowed) {
                alertIsShowed = TRUE;
                [AnimationsFX showConnectionErrorView:view toShow:TRUE];
            }
        });
    };
    
    [internetReachable startNotifier];
    return !alertIsShowed;
}

@end
