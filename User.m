//
//  User.m
//  Glance
//
//  Created by FFabio on 29/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize userFacebookID = _userFacebookID, userFacebookName = _userFacebookName, userFacebookPhoto = _userFacebookPhoto, userFacebookPhotoLink = _userFacebookPhotoLink;

- (id) initWithFBID:(NSString *)userFBID FBName:(NSString *)userFBName FBPhotoLink:(NSString *)userFBPhotoLink
{
    self = [super init];
    if (self) {
        self.userFacebookID = userFBID;
        self.userFacebookName = userFBName;
        self.userFacebookPhoto = [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:userFBPhotoLink]]];
    }
    return self;
}

@end
