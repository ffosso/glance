//
//  PostDetails_VC.m
//  Glance
//
//  Created by FFabio on 10/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "PostDetails_VC.h"

@interface PostDetails_VC ()

@end

@implementation PostDetails_VC

@synthesize commentTextField, sendComment, spotName, mainTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    commentTextField.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tap.cancelsTouchesInView = FALSE;
    [self.view addGestureRecognizer:tap];
    
    dataCenter = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getDataCenter];
//    waManager = [[WebApiManager alloc] init];
    waManager = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getWebApiManager];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    dataCenter.commentList = [waManager getPostCommetsByPostID:[NSString stringWithFormat:@"%d", post.idPost]];
    spotName.text = spot.name;
/*    NSString *pippo = @"AA";
    label.frame = [AnimationsFX calculateHeightOfUIlabelByText:pippo andLabel:label];
    label.numberOfLines = [AnimationsFX calculateNumberLinesOfUIlabelByText:pippo andLabel:label];
    label.text = pippo;*/
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:nil object:nil];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (displayKeyboard) [commentTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Delegate Methods
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // +1 = cella con i dettagli del post
    return dataCenter.commentList.count+1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == FIRSTCELL) {
        if (![post.postPhotoURL isEqual:[NSNull null]]) {
            PostCellWithPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_PHOTO_POST_ID];
            
            cell.iLikeButton.enabled = TRUE;
            if (![post.iLikeEnabled isEqual:[NSNull null]])
                cell.iLikeButton.enabled = FALSE;
            
            cell.postPhoto.image = [UIImage imageNamed:@"postPhotoPlaceHolder.png"];
            cell.userPhoto.image = [UIImage imageNamed:@"defaultAnonymous.png"];
            
            cell.userName.text = ([post.userName isEqual:[NSNull null]] && [post.userSurname isEqual:[NSNull null]]) ? @"Anonimo" : [NSString stringWithFormat:@"%@ %@", post.userName, post.userSurname];
            
            cell.postBody.frame = [AnimationsFX calculateHeightOfUIlabelByText:post.body andLabel:cell.postBody];
            cell.postBody.numberOfLines = [AnimationsFX calculateNumberLinesOfUIlabelByText:post.body andLabel:cell.postBody];
            cell.postBody.text = post.body;
            cell.postTime.text = post.timeStamp;
            
            NSString *nLikes = ([post.numberLike isEqual:[NSNull null]]) ? @"0" : post.numberLike;
            NSString *nComments = ([post.numberComment isEqual:[NSNull null]]) ? @"0" : post.numberComment;
            if ([nComments isEqualToString:@"1"])
                cell.numberLikeAndComment.text = [NSString stringWithFormat:@"%@ Mi piace - %@ Commento", nLikes, nComments];
            else
                cell.numberLikeAndComment.text = [NSString stringWithFormat:@"%@ Mi piace - %@ Commenti", nLikes, nComments];
            
            if (![post.userPhotoURL isEqual:[NSNull null]])
                [self downloadUserPhotoAsyncByIndexPath:indexPath];
            [self downloadImagesAsyncByIndexPath:indexPath];
            
            
            return cell;
        }
        
        PostCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_POST_ID];
        
        cell.iLikeButton.enabled = TRUE;
        if (![post.iLikeEnabled isEqual:[NSNull null]])
            cell.iLikeButton.enabled = FALSE;
        
        cell.userPhoto.image = [UIImage imageNamed:@"defaultAnonymous.png"];
        
        cell.userName.text = ([post.userName isEqual:[NSNull null]] && [post.userSurname isEqual:[NSNull null]]) ? @"Anonimo" : [NSString stringWithFormat:@"%@ %@", post.userName, post.userSurname];
        
        cell.postBody.frame = [AnimationsFX calculateHeightOfUIlabelByText:post.body andLabel:cell.postBody];
        cell.postBody.numberOfLines = [AnimationsFX calculateNumberLinesOfUIlabelByText:post.body andLabel:cell.postBody];
        cell.postBody.text = post.body;
        cell.postTime.text = post.timeStamp;
        
        NSString *nLikes = ([post.numberLike isEqual:[NSNull null]]) ? @"0" : post.numberLike;
        NSString *nComments = ([post.numberComment isEqual:[NSNull null]]) ? @"0" : post.numberComment;
        if ([nComments isEqualToString:@"1"])
            cell.numberLikeAndComment.text = [NSString stringWithFormat:@"%@ Mi piace - %@ Commento", nLikes, nComments];
        else
            cell.numberLikeAndComment.text = [NSString stringWithFormat:@"%@ Mi piace - %@ Commenti", nLikes, nComments];
        
        if (![post.userPhotoURL isEqual:[NSNull null]])
            [self downloadUserPhotoAsyncByIndexPath:indexPath];
        
        return cell;
    }
    else {
        // -1 perchè la prima cella è occupata dal post
        Comment *comment = [dataCenter.commentList objectAtIndex:indexPath.row-1];
        CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_POST_ID];
        if (![comment.userPhotoURL isEqual:[NSNull null]])
            [self downloadUserPhotoAsyncByIndexPath:indexPath];
        
        cell.comment.frame = [AnimationsFX calculateHeightOfUIlabelByText:comment.body andLabel:cell.comment];
        cell.comment.numberOfLines = [AnimationsFX calculateNumberLinesOfUIlabelByText:comment.body andLabel:cell.comment];
        cell.comment.text = comment.body;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 72.0 e 53.0 = Altezze di default inserita da xib
    if (indexPath.row == FIRSTCELL) {
        if (![post.postPhotoURL isEqual:[NSNull null]]) {
            PostCellWithPhotoCell *cell = (PostCellWithPhotoCell*) [tableView cellForRowAtIndexPath:indexPath];
            cell.postBody.frame = [AnimationsFX calculateHeightOfUIlabelByText:post.body andLabel:cell.postBody];
            return POSTCELLWITHPHOTO_HEIGHT + (cell.postBody.frame.size.height - 72.0);
        }
        else {
            PostCell *cell = (PostCell*) [tableView cellForRowAtIndexPath:indexPath];
            cell.postBody.frame = [AnimationsFX calculateHeightOfUIlabelByText:post.body andLabel:cell.postBody];
            return POSTCELL_HEIGHT + (cell.postBody.frame.size.height - 72.0);
        }
    }
    else {
        // -1 because the first cell is filled by Post data
        Comment *comment = [dataCenter.commentList objectAtIndex:indexPath.row-1];
        CommentCell *cell = (CommentCell*) [tableView cellForRowAtIndexPath:indexPath];
        cell.comment.frame = [AnimationsFX calculateHeightOfUIlabelByText:comment.body andLabel:cell.comment];
        return COMMENTCELL_HEIGHT + (cell.comment.frame.size.height - 53.0);
    }
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    /*    if ( textField.bounds.origin.y > IPAD_WIDTH-(352+10) ) {
     [UIView animateWithDuration:0.2 animations:^{
     self.view.transform = CGAffineTransformTranslate(self.view.transform, 0, textField.frame.origin.y-(416+10));
     }];
     }*/
    activeUITextField = textField;
    activeUITextFieldOriginY = textField.frame.origin.y;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    /*    if ( textField.bounds.origin.y > 768-(352+10) ) {
     [UIView animateWithDuration:0.2 animations:^{
     self.view.transform = CGAffineTransformTranslate(self.view.transform, 0, (416+10)-textField.frame.origin.y);
     }];
     }*/
    activeUITextField = nil;
    activeUITextFieldOriginY = -1;
}

- (void) keyboardWillShow:(NSNotification *)aNotification
{
    NSDictionary* userInfo = [aNotification userInfo];
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if ( activeUITextFieldOriginY+activeUITextField.frame.size.height > keyboardEndFrame.origin.y )
        [AnimationsFX showViewByTraslation:self.view traslationX:0 traslationY:(keyboardEndFrame.origin.y-(activeUITextFieldOriginY+64+activeUITextField.frame.size.height)) duration:0.25];
}

- (void) keyboardWillHide:(NSNotification *)aNotification
{
    [AnimationsFX showViewByTraslation:self.view traslationX:0 traslationY:0 duration:0.25];
}

#pragma mark - Inner Methods

- (void) hideKeyboard
{
    [self.view endEditing:TRUE];
}

- (void) setPostDetails:(Post *)postDetails andSpot:(Spot *)spotDetails
{
    post = postDetails;
    spot = spotDetails;
}

- (void) openKeyboard:(BOOL)open
{
    // +1 NO perchè .row indica la cella specifica non quante!
    if (open)
        [mainTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:dataCenter.commentList.count inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
    displayKeyboard = open;
}

- (void) downloadImagesAsyncByIndexPath:(NSIndexPath *)indexPath
{
//    Post *post = [dataCenter.postList objectAtIndex:indexPath.row];
    if (![post.postPhotoURL isEqual:[NSNull null]]) {
        __block NSData *imageData = [cachePostPhotos objectForKey:post.postPhotoURL];
        if (imageData) {
            [(PostCellWithPhotoCell *) [mainTable cellForRowAtIndexPath:indexPath] postPhoto].image = [UIImage imageWithData:imageData];
        } else {
            dispatch_queue_t imageQueue_ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
            dispatch_async(imageQueue_, ^{
                if (mainTable.dragging == FALSE && mainTable.decelerating == FALSE) {
                    imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:post.postPhotoURL]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (imageData) {
                            [cachePostPhotos setObject:imageData forKey:post.postPhotoURL];
                            [(PostCellWithPhotoCell *) [mainTable cellForRowAtIndexPath:indexPath] postPhoto].image = [UIImage imageWithData:imageData];
                        }
                    });
                }
            });
        }
    }
}

- (void) downloadUserPhotoAsyncByIndexPath:(NSIndexPath *) indexPath
{
//    Post *post = [dataCenter.postList objectAtIndex:indexPath.row];
    if (![post.userPhotoURL isEqual:[NSNull null]]) {
        __block NSData *userPhotoData = [cachePostUserPhotos objectForKey:post.userPhotoURL];
        if (userPhotoData) {
            if (![post.postPhotoURL isEqual:[NSNull null]])
                [(PostCellWithPhotoCell *) [mainTable cellForRowAtIndexPath:indexPath] userPhoto].image = [UIImage imageWithData:userPhotoData];
            else
                [(PostCell *) [mainTable cellForRowAtIndexPath:indexPath] userPhoto].image = [UIImage imageWithData:userPhotoData];
        } else {
            dispatch_queue_t imageQueue_ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
            dispatch_async(imageQueue_, ^{
                if (mainTable.dragging == FALSE && mainTable.decelerating == FALSE) {
                    userPhotoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:post.userPhotoURL]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cachePostUserPhotos setObject:userPhotoData forKey:post.userPhotoURL];
                        if (![post.postPhotoURL isEqual:[NSNull null]]) {
                            if (userPhotoData)
                                [(PostCellWithPhotoCell *) [mainTable cellForRowAtIndexPath:indexPath] userPhoto].image = [UIImage imageWithData:userPhotoData];
                        }
                        else {
                            if (userPhotoData)
                                [(PostCell *) [mainTable cellForRowAtIndexPath:indexPath] userPhoto].image = [UIImage imageWithData:userPhotoData];
                        }
                    });
                }
            });
        }
    }
}

- (void) reloadCellImages
{
    NSArray *visiblePath = [mainTable indexPathsForVisibleRows];
    for (NSIndexPath *indexPath in visiblePath) {
        [self downloadImagesAsyncByIndexPath:indexPath];
        [self downloadUserPhotoAsyncByIndexPath:indexPath];
    }
}

#pragma mark - IBAction Methods

- (IBAction) sendILike:(id)sender
{
    [waManager sendILikeWithUserID:USER_ID_TEST postID:[NSString stringWithFormat:@"%d", post.idPost] commentID:Nil];
    ((UIButton *) sender).userInteractionEnabled = FALSE;
    dataCenter.postList = [waManager getPostBySpot:[NSString stringWithFormat:@"%d", spot.idSpot]];
    [mainTable reloadData];
    [self reloadCellImages];
}

@end
