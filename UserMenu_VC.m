//
//  UserMenu_VC.m
//  Glance
//
//  Created by FFabio on 20/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "UserMenu_VC.h"

@interface UserMenu_VC ()

@end

@implementation UserMenu_VC

@synthesize alertBannerView, errorConnectionView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    checkConnectionTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(checkConnection) userInfo:nil repeats:TRUE];
    checkConnection = [[CheckConnection alloc] init];
    
    dataCenter = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getDataCenter];
	// Do any additional setup after loading the view.
}

- (void) viewDidUnload
{
    [super viewDidUnload];
    [checkConnectionTimer invalidate];
    checkConnectionTimer = Nil;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [checkConnection checkConnectionAndShowConnectionError:errorConnectionView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Delegate methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return dataCenter.spotListTipology.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ (SpotTipology*) [dataCenter.spotListTipology objectAtIndex:section] number];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    NSArray *spotByTipology = [[dataCenter getSpotListDividedInTipology] objectForKey:[ (SpotTipology*) [dataCenter.spotListTipology objectAtIndex:indexPath.section] name]];
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_MENU_ID];
    cell.spotName.text = [(Spot *) [spotByTipology objectAtIndex:indexPath.row] name];
    return  cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [ (SpotTipology*) [dataCenter.spotListTipology objectAtIndex:section] name];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *spotByTipology = [[dataCenter getSpotListDividedInTipology] objectForKey:[ (SpotTipology*) [dataCenter.spotListTipology objectAtIndex:indexPath.section] name]];
    
    Room_VC *roomVC = (Room_VC *) [[(UINavigationController *) self.presentingViewController viewControllers] lastObject];
    [self dismissViewControllerAnimated:TRUE completion:^{
        [roomVC reloadDataBySpot:(Spot *) [spotByTipology objectAtIndex:indexPath.row] andLastPostID:Nil];
    }];
}

#pragma mark - Inner Methods

- (void) checkConnection
{
//    [checkConnection checkConnectionAndShowBanner:alertBannerView];
    [checkConnection checkConnectionAndShowConnectionError:errorConnectionView];
}

@end
