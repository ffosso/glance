//
//  SendPost_VC.m
//  Glance
//
//  Created by FFabio on 05/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "SendPost_VC.h"

@interface SendPost_VC ()

@end

@implementation SendPost_VC

@synthesize spinner, errorConnectionView, spotName, inputBody, anonymousOrNot, photo, userName, userPhoto;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    inputBody.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tap.cancelsTouchesInView = FALSE;
    [self.view addGestureRecognizer:tap];
    //    waManager = [[WebApiManager alloc] init];
    waManager = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getWebApiManager];
    ManageLogin *mLogin = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getLoginManager];
    user = [mLogin getUserInfo];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    spotName.text = spot.name;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Inner Methods

- (UIImage*) resizeImage:(UIImage*)image
{
    if (image.size.width > MAX_IMAGE_SIDE_SIZE || image.size.height > MAX_IMAGE_SIDE_SIZE) {
        // Le dimensioni sono espresse in MP
        float sideResized = (MIN(image.size.width, image.size.height)*MAX_IMAGE_SIDE_SIZE)/MAX(image.size.width, image.size.height);
        float newWidth = (image.size.width >= image.size.height) ? MAX_IMAGE_SIDE_SIZE : sideResized;
        float newHeight = (image.size.height >= image.size.width) ? MAX_IMAGE_SIDE_SIZE : sideResized;
        
        CGSize newSize = CGSizeMake(newWidth, newHeight);
        UIGraphicsBeginImageContext( newSize );
        [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
        NSData *dataForJPEGFile = UIImageJPEGRepresentation(UIGraphicsGetImageFromCurrentImageContext(), 0.9);
        UIGraphicsEndImageContext();
        //NSLog(@"Ow-Ol: %f - %f , Nw-Nl: %f - %f, dataForJPEGFile: %.2d, PNG: ",image.size.width, image.size.height, newWidth, newHeight, dataForJPEGFile.length/1024);
        return [UIImage imageWithData:dataForJPEGFile];
    }
    return image;
}

- (void) hideKeyboard
{
    [self.view endEditing:TRUE];
}

- (void) loadSpotData:(Spot *)s
{
    spot = [[Spot alloc] initWithID:s.idSpot name:s.name city:Nil tipology:Nil latitude:Nil longitude:Nil date:Nil];
}

#pragma mark - IBAction Methods

- (IBAction) takePhoto:(id)sender
{
    [self presentModalViewController:imagePicker animated:YES];
}

- (IBAction) sendPost:(id)sender
{
    if ([inputBody.text isEqualToString:@"Inserisci il testo"])
        [AnimationsFX showAlertDialogWithTitle:@"Errore" message:@"Per inviare il post è necessario inserire il testo" firstButtonTitle:@"Chiudi" secondButtonTitle:Nil];
    else {
//        [self dismissModalViewControllerAnimated:YES];
        PostToSend *postToSend = [[PostToSend alloc] initWithID:[NSString stringWithFormat:@"%d", spot.idSpot]
                                                         userID:user.userFacebookID
                                                           body:inputBody.text
                                                      anonimous:(anonymousOrNot.on) ? [NSString stringWithFormat:@"T"] : [NSString stringWithFormat:@"F"]
                                                      postPhoto:(photo.image) ? photo.image : Nil];
        [waManager sendPost:postToSend];
        [self.navigationController popViewControllerAnimated:TRUE];
/*        Room_VC *roomVC = (Room_VC *) [[(UINavigationController *) self.presentingViewController viewControllers] lastObject];
        [self dismissViewControllerAnimated:TRUE completion:^{
           [roomVC reloadDataBySpot:spot andLastPostID:Nil];
        }];*/
    }
}

- (IBAction) closeSendPost:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction) selectAnonymousOrNot:(UISwitch *)sender
{
    if ([sender isOn]) {
        userPhoto.image = [UIImage imageNamed:@"defaultAnonymous.png"];
        userName.text = @"Anonimo";
    }
    else {
        userPhoto.image = user.userFacebookPhoto;
        userName.text = user.userFacebookName;
    }
}

#pragma mark - Delegate Methods

- (void) imagePickerControllerDidCancel:(UIImagePickerController *) picker
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void) imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *) info {
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    photo.image = [self resizeImage:image];
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void) textViewDidBeginEditing:(UITextView *)textView
{
    inputBody.text = @"";
}

@end
