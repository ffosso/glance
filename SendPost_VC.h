//
//  SendPost_VC.h
//  Glance
//
//  Created by FFabio on 05/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#define MAX_IMAGE_SIDE_SIZE 1280.0

#import <UIKit/UIKit.h>
#import "Room_VC.h"
#import "PostToSend.h"

@interface SendPost_VC : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate> {
    
    @private
    UIImagePickerController * imagePicker;
    Spot *spot;
    WebApiManager *waManager;
    User *user;
    
}

@property(strong, nonatomic) IBOutlet UIButton *facebookLoginBtn;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property(nonatomic, strong) IBOutlet UIView *errorConnectionView;


@property(nonatomic, strong) IBOutlet UIImageView *userPhoto;
@property(nonatomic, strong) IBOutlet UILabel *userName;
@property(nonatomic, strong) IBOutlet UILabel *spotName;
@property(nonatomic, strong) IBOutlet UITextView *inputBody;
@property(nonatomic, strong) IBOutlet UISwitch *anonymousOrNot;
@property(nonatomic, strong) IBOutlet UIImageView *photo;

- (void) loadSpotData:(Spot *)s;
- (UIImage*) resizeImage:(UIImage*)image;

- (IBAction) takePhoto:(id)sender;
- (IBAction) sendPost:(id)sender;
- (IBAction) closeSendPost:(id)sender;
- (IBAction) selectAnonymousOrNot:(UISwitch *)sender;

@end
