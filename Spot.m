//
//  Spot.m
//  Glance
//
//  Created by FFabio on 19/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "Spot.h"

@implementation Spot

@synthesize idSpot = _idSpot, name = _name, city = _city, tipology = _tipology, location = _location, timestampCreation = _timestampCreation;

- (id)initWithID:(NSUInteger)idSpot name:(NSString *)name city:(NSString *)city tipology:(NSString *)tipology latitude:(NSString *)latitude longitude:(NSString *)longitude date:(NSString *)timestampCreation;
{
    self = [super init];
    if (self) {
        _idSpot = idSpot;
        _name = name;
        _city = city;
        _tipology = tipology;
        _timestampCreation = timestampCreation;
        _location = [_location initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
    }
    return self;
}

@end
