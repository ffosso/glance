//
//  MenuCell.h
//  Glance
//
//  Created by FFabio on 20/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuCell : UITableViewCell

@property(strong, nonatomic) IBOutlet UILabel *spotName;

@end
