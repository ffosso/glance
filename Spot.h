//
//  Spot.h
//  Glance
//
//  Created by FFabio on 19/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Spot : NSObject

@property(readonly) NSUInteger idSpot;
@property(strong, nonatomic, readonly) NSString *name;
@property(strong, nonatomic, readonly) NSString *city;
@property(strong, nonatomic, readonly) NSString *tipology;
@property(strong, nonatomic, readonly) CLLocation *location;
@property(strong, nonatomic, readonly) NSString *timestampCreation;

- (id)initWithID:(NSUInteger)idSpot name:(NSString *)name city:(NSString *)city tipology:(NSString *)tipology latitude:(NSString *)latitude longitude:(NSString *)longitude date:(NSString *)timestampCreation;

@end
