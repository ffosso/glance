//
//  FbLoginViewController.h
//  Glance
//
//  Created by FFabio on 30/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FbLoginViewController : UIViewController

- (IBAction) performLogin:(id)sender;

@end
