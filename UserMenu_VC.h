//
//  UserMenu_VC.h
//  Glance
//
//  Created by FFabio on 20/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Room_VC.h"
#import "MenuCell.h"

@interface UserMenu_VC : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
    @private
    DataCenter *dataCenter;
    
    NSTimer *checkConnectionTimer;
    CheckConnection *checkConnection;
    
}

@property(nonatomic, strong) IBOutlet UITableView *tableMenu;
@property(nonatomic, strong) IBOutlet UIView *alertBannerView;
@property(nonatomic, strong) IBOutlet UIView *errorConnectionView;

@end
