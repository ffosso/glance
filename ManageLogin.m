//
//  ManageLogin.m
//  Glance
//
//  Created by FFabio on 02/05/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "ManageLogin.h"

@implementation ManageLogin 

- (id) init
{
    self = [super init];
    if (self) {
        condition = [NSCondition new];
        questionAnswered = FALSE;
        user = Nil;
    }
    return self;
}

#pragma mark - Public Methods

- (User *) getUserInfo
{
    //If user is not loggen in
    if (!user) {
//        [self performSelectorInBackground:@selector(showAlertView) withObject:Nil];
        [self performSelectorOnMainThread:@selector(showAlertView) withObject:Nil waitUntilDone:NO];
        [condition lock];
        while (!questionAnswered) [condition wait];
        questionAnswered = FALSE;
        [condition unlock];
    }
    // AAA
    return user;
}

#pragma mark - Delegate Methods

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
        user = Nil;
    else {
        // FBSession
        if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
            // To-do, show logged in view
            [self openSession];
        } else {
            // No, display the login page.
            //        [self showLoginView];
            [self openSession];
        }
    }
    user = [[User alloc] initWithFBID:@"007" FBName:@"FB_UserName" FBPhotoLink:@"http://th02.deviantart.net/fs71/PRE/i/2011/122/8/2/sexy_facebook_avatar_by_tesne-d3feuml.jpg"];
    questionAnswered = YES;
    [condition signal];
}

#pragma mark - Facebook Methods

- (void) sessionStateChanged:(FBSession *)session
                       state:(FBSessionState) state
                       error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: {
            /* UIViewController *topViewController =
             [self.navController topViewController];
             if ([[topViewController modalViewController]
             isKindOfClass:[SCLoginViewController class]]) {
             [topViewController dismissModalViewControllerAnimated:YES];
             }*/
            NSLog(@"AAA!");
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed: {
            // Once the user has logged in, we want them to
            // be looking at the root view.
            //[self.navController popToRootViewControllerAnimated:NO];
            UIAlertView *avLoginFailed = [[UIAlertView alloc]
                                          initWithTitle:@"Attenzione autenticazione fallitta."
                                          message:error.localizedDescription
                                          delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
            [avLoginFailed show];
            [FBSession.activeSession closeAndClearTokenInformation];
        }
            //[self showLoginView];
            break;
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertViewError = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertViewError show];
    }
}

- (void) openSession
{
    [FBSession openActiveSessionWithReadPermissions:nil
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         [self sessionStateChanged:session state:state error:error];
     }];
}

- (void) showAlertView
{
    alertView = [[UIAlertView alloc]
                 initWithTitle:@"Autorizzazione"
                 message:@"Vuoi effettuare il login tramite Facebook?"
                 delegate:self
                 cancelButtonTitle:@"No"
                 otherButtonTitles:@"Si", nil];
    [alertView show];
}

@end
