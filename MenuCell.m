//
//  MenuCell.m
//  Glance
//
//  Created by FFabio on 20/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

@synthesize spotName = _spotName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
