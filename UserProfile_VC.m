//
//  UserProfile_VC.m
//  Glance
//
//  Created by FFabio on 15/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "UserProfile_VC.h"

@interface UserProfile_VC ()

@end

@implementation UserProfile_VC

@synthesize facebookLoginBtn, spinner, errorConnectionView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [checkConnection checkConnectionAndShowConnectionError:errorConnectionView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Inner Methods

/*- (IBAction) makeLogin:(id)sender
{
    [self.spinner startAnimating];
    
    AppDelegate *appDelegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [appDelegate openSession];
}*/

@end
