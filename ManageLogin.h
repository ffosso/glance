//
//  ManageLogin.h
//  Glance
//
//  Created by FFabio on 02/05/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>
#import "FbLoginViewController.h"
#import "User.h"

@interface ManageLogin : NSObject <UIAlertViewDelegate> {
    
    @private
    User *user;
    UIAlertView *alertView;
    BOOL questionAnswered;
    NSCondition *condition;
}

//+ (void) checkLogin;
- (id) init;
- (User *) getUserInfo;
//- (User *) makeLogin;

@end
