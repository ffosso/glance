//
//  Comment.h
//  Glance
//
//  Created by FFabio on 17/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject

@property(strong, nonatomic, readonly) NSString *commentID;
@property(strong, nonatomic, readonly) NSString *body;
@property(strong, nonatomic, readonly) NSString *timestamp;
@property(strong, nonatomic, readonly) NSString *userID;
@property(strong, nonatomic, readonly) NSString *postID;
@property(strong, nonatomic, readonly) NSString *userPhotoURL;

- (id) initWithCommentID:(NSString *)commentID body:(NSString *)body timestamp:(NSString *)timestamp userID:(NSString *)userID postID:(NSString *)postID userPhotoURL:(NSString *)userPhotoURL;

@end
