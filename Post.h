//
//  Post.h
//  Glance
//
//  Created by FFabio on 21/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Post : NSObject

@property(readonly) NSUInteger idPost;
@property(strong, nonatomic, readonly) NSString *userName;
@property(strong, nonatomic, readonly) NSString *facebookUserID;
@property(strong, nonatomic, readonly) NSString *userSurname;
@property(strong, nonatomic, readonly) NSString *body;
@property(strong, nonatomic, readonly) NSString *numberLike;
@property(strong, nonatomic, readonly) NSString *numberComment;
@property(strong, nonatomic, readonly) NSString *timeStamp;
@property(strong, nonatomic, readonly) NSString *userPhotoURL;
@property(strong, nonatomic, readonly) NSString *postPhotoURL;
@property(strong, nonatomic, readonly) NSString *iLikeEnabled;

- (id)initWithID:(NSUInteger)idPost userName:(NSString *)userName userSurame:(NSString *)userSurame facebookUserID:(NSString *)facebookUserID body:(NSString *)body numberLike:(NSString *)numberLike numberComment:(NSString *)numberComment timeStamp:(NSString *)timeStamp userPhotoURL:(NSString *)userPhotoURL postPhotoURL:(NSString *)postPhotoURL iLikeEnabled:(NSString *)iLikeEnabled;

@end
