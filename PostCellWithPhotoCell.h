//
//  PostCellWithPhotoCell.h
//  Glance
//
//  Created by FFabio on 22/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCellWithPhotoCell : UITableViewCell

@property(strong, nonatomic) IBOutlet UIImageView *userPhoto;
@property(strong, nonatomic) IBOutlet UILabel *userName;
@property(strong, nonatomic) IBOutlet UILabel *postTime;
@property(strong, nonatomic) IBOutlet UILabel *numberLikeAndComment;
@property(strong, nonatomic) IBOutlet UILabel *postBody;
@property(strong, nonatomic) IBOutlet UIImageView *postPhoto;
@property(strong, nonatomic) IBOutlet UIButton *iLikeButton;

@end
