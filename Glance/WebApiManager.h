//
//  WebApiManager.h
//  Glance
//
//  Created by FFabio on 19/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#ifndef DEBUGS
    #define DEBUGS
#endif

#import <Foundation/Foundation.h>
#import "Spot.h"
#import "SpotTipology.h"
#import "Post.h"
#import "PostToSend.h"

@class WebApiManager;
@protocol WebApiManagerDelegate <NSObject>
    //@required
    - (void) sendPostCompletion:(NSError*) error;
@end

@interface WebApiManager : NSObject
{
     NSMutableArray *delegates;
}

//@property (assign, nonatomic) id <WebApiManagerDelegate> delegate;

- (id) init;

//Delegate Array
- (void) addDelegate:(id) object;
- (void) removeDelegate:(id) object;

// GET
- (NSArray *) getSpotTipologyList;
- (NSArray *) getSpotList;
- (NSArray *) getPostBySpot:(NSString *)spotId;
- (NSArray *) getPostCommetsByPostID:(NSString *)postID;
- (NSDictionary *) getPostBySpot:(NSString *)spotId andLastPostId:(NSString *)postID;
- (NSDictionary *) getPostByGPSWithLatitude:(NSString *)latitude andLongitude:(NSString *)longitude;

// SEND
- (NSString *) sendILikeWithUserID:(NSString *)userID postID:(NSString *)postID commentID:(NSString *)commentID;
- (void) sendPost:(PostToSend *)postToSend;
//- (void) sendPhotoOfPost:(PostToSend *)postToSend withImage:(UIImage *)photo;

@end

#ifdef DEBUGS
#undef DEBUGS
#endif