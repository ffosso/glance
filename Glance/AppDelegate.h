//
//  AppDelegate.h
//  Glance
//
//  Created by FFabio on 01/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "DataCenter.h"
#import "WebApiManager.h"
#import "ManageLogin.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate> {
    
    @private
    DataCenter *dataCenter;
    WebApiManager *waManager;
    ManageLogin *mLogin;
//    User *user;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CLLocationManager *locationManager;

//- (void) openSession;
- (DataCenter *) getDataCenter;
- (WebApiManager *) getWebApiManager;
- (ManageLogin *) getLoginManager;
//- (User *) getUserInfo;

@end
