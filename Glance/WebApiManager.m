//
//  WebApiManager.m
//  Glance
//
//  Created by FFabio on 19/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "WebApiManager.h"

@implementation WebApiManager

- (id) init
{
    self = [super init];
    if (self) {
        delegates = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark - Delegate Array

- (void) addDelegate:(id) object
{
    [delegates addObject:object];
}

- (void) removeDelegate:(id) object
{
    [delegates removeObject:object];
}

- (void) notifyDelegates:(NSError *)error
{
    for (int i=0; i<delegates.count; i++)
        [[delegates objectAtIndex:i] sendPostCompletion:error];
}

#pragma mark - GET Public Methods 

- (NSArray *) getSpotTipologyList
{
    NSURL *url = [[NSURL alloc] initWithString:SERVER_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:SPOT_TIPOLOGY_LIST_APIURL parameters:nil];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseObject = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSMutableArray *returnSpotTipologyList = Nil;
    
    // !error = No errors
    if (!error) {
        NSDictionary *jsonDict = (NSDictionary*) [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        #ifndef DEBUGS
//        NSLog(@"%@", jsonDict);
        #endif
        NSArray *spotTipologyList = [jsonDict objectForKey:@"tipologia"];
        returnSpotTipologyList = [[NSMutableArray alloc] initWithCapacity:spotTipologyList.count];
        [spotTipologyList enumerateObjectsUsingBlock:^(id obj,NSUInteger idx, BOOL *stop) {
             SpotTipology *spotTypology = [[SpotTipology alloc]
                                       initWithID:0
                                             name:[obj objectForKey:@"TIPOLOGIA"]
                                           number:[[obj objectForKey:@"NUMERO"] intValue]];
            [returnSpotTipologyList addObject:spotTypology];
        }];
        #ifndef DEBUGS
//        NSLog(@"returnSpotTipologyList.count: %d", returnSpotTipologyList.count);
        #endif
    }
    
    return returnSpotTipologyList;    
}

- (NSArray *) getSpotList
{
    NSURL *url = [[NSURL alloc] initWithString:SERVER_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:SPOT_LIST_APIURL parameters:nil];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseObject = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSMutableArray *returnSpotList = Nil;
    
    // !error = No errors
    if (!error) {
        NSDictionary *jsonDict = (NSDictionary*) [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        #ifndef DEBUGS
//        NSLog(@"%@", jsonDict);
        #endif
        NSArray *spotList = [jsonDict objectForKey:@"luoghi"];
        returnSpotList = [[NSMutableArray alloc] initWithCapacity:spotList.count];
        [spotList enumerateObjectsUsingBlock:^(id obj,NSUInteger idx, BOOL *stop) {
            Spot *spot = [[Spot alloc] initWithID:[[obj objectForKey:@"IDLUOGO"] integerValue]
                                             name:[obj objectForKey:@"NOME"]
                                             city:[obj objectForKey:@"CITTA"]
                                         tipology:[obj objectForKey:@"TIPOLOGIA"]
                                         latitude:[obj objectForKey:@"LATITUDINE"]
                                        longitude:[obj objectForKey:@"LONGITUDINE"]
                                             date:[obj objectForKey:@"DATACREAZIONE"]];
            [returnSpotList addObject:spot];
        }];
        #ifndef DEBUGS
        NSLog(@"returnSpotList.count: %d", returnSpotList.count);
        #endif
    }
    
    return returnSpotList;
}

- (NSArray *) getPostCommetsByPostID:(NSString *)postID
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:postID, @"post", nil];
    NSArray *commentList = [[self getDataOfPostByParams:params] objectForKey:@"commenti"];
    return commentList;
}

- (NSArray *) getPostBySpot:(NSString *)spotId
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:USER_ID_TEST, @"utente", spotId, @"luogo", nil];
    NSArray *postList = [[self getDataOfPostByParams:params] objectForKey:@"PostList"];
    return postList;
}

- (NSDictionary *) getPostBySpot:(NSString *)spotId andLastPostId:(NSString *)postID
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:USER_ID_TEST, @"utente", spotId, @"luogo", postID, @"ultimoPostID", nil];
    return [self getDataOfPostByParams:params];
}

- (NSDictionary *) getPostByGPSWithLatitude:(NSString *)latitude andLongitude:(NSString *)longitude
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:USER_ID_TEST, @"utente", latitude, @"latitudine", longitude, @"longitudine", nil];
    return [self getDataOfPostByParams:params];
}

#pragma mark - SEND Public Methods

- (NSString *) sendILikeWithUserID:(NSString *)userID postID:(NSString *)postID commentID:(NSString *)commentID
{
    NSString *message;
    
    NSURL *url = [[NSURL alloc] initWithString:SERVER_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:userID, @"utente", postID, @"post", commentID, @"commento", nil];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:I_LIKE_APIURL parameters:params];
    NSURLResponse *response = nil;
    NSError *error = nil;
    /*NSData *responseObject = */[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    return message;
}

/*- (void) sendPhotoOfPost:(PostToSend *)postToSend withImage:(UIImage *)photo
{
    NSURL *url = [[NSURL alloc] initWithString:SERVER_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    // DA CORREGGERE MANCA IMMAGINE
//    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:postToSend.spotID, @"luogo", postToSend.userID, @"utente", postToSend.body, @"corpo", postToSend.anonimousMode, @"anonimo", nil];
    
    NSData *dataObj = UIImageJPEGRepresentation(postToSend.postPhoto, 1.0);
    NSMutableURLRequest *afRequest = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                           path:SEND_POST_URL
                                                                     parameters:Nil
                                                      constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                                       {
                                          [formData appendPartWithFileData:dataObj
                                                                      name:@"A"
                                                                  fileName:@"B"
                                                                  mimeType:@"image/jpeg"];
                                      }];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    operation.completionBlock = ^ {
        [AnimationsFX showAlertDialogWithTitle:@"Response" message:operation.responseString firstButtonTitle:@"Chiudi" secondButtonTitle:Nil];
    };
    [operation start];
}*/

- (void) sendPost:(PostToSend *)postToSend
{
    NSURL *url = [[NSURL alloc] initWithString:SERVER_URL];
/*    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:postToSend.spotID, @"luogo", postToSend.userID, @"utente", postToSend.body, @"corpo", postToSend.anonimousMode, @"anonimo", nil];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:SEND_POST_URL parameters:params];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseObject = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
 NSLog(@"########### ---> sendPost response %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);*/
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:postToSend.spotID, @"luogo", postToSend.userID, @"utente", postToSend.body, @"corpo", postToSend.anonimousMode, @"anonimo", nil];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableURLRequest *request;
    if (postToSend.postPhoto == Nil) {
        request = [httpClient requestWithMethod:@"POST" path:SEND_POST_URL parameters:params];
        /*NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *responseObject = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];*/
    }
    else {
        //    [httpClient setParameterEncoding:AFFormURLParameterEncoding];
        NSData *imageData = UIImageJPEGRepresentation(postToSend.postPhoto, 1.0);
        NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970];
        NSString *generateFileName = [[NSString alloc] initWithFormat:@"%@_%@%@", postToSend.userID, [NSDate dateWithTimeIntervalSince1970:timeInterval], PHOTO_EXTENTION];
        
        generateFileName = [generateFileName stringByReplacingOccurrencesOfString:@"-" withString:@""];
        generateFileName = [generateFileName stringByReplacingOccurrencesOfString:@" " withString:@""];
        generateFileName = [generateFileName stringByReplacingOccurrencesOfString:@":" withString:@""];
        generateFileName = [generateFileName stringByReplacingOccurrencesOfString:@"+" withString:@""];
        
        request = [httpClient multipartFormRequestWithMethod:@"POST" path:SEND_POST_URL parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
            [formData appendPartWithFileData:imageData name:@"uploadedfile" fileName:generateFileName mimeType:@"image/jpeg"];
        }];
        /*AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.completionBlock = ^ {
    //        [AnimationsFX showAlertDialogWithTitle:request.URL message:operation.responseString firstButtonTitle:@"Chiudi" secondButtonTitle:Nil];
        };
        [operation start];*/
    }
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [self notifyDelegates:error];
     }];
}

#pragma mark - Inner Methods

- (NSDictionary *) getDataOfPostByParams:(NSDictionary *)params
{
    NSDictionary *dictionaryToReturn;
    NSString *currentSpotName;
    NSString *idCurrentSpot;
    NSString *numberOfPost;
    
    NSURL *url = [[NSURL alloc] initWithString:SERVER_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:POST_BY_SPOTID_APIURL parameters:params];
    NSLog(@"request.URL: %@", request.URL);
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseObject = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSMutableArray *returnPostList = Nil;
    
    // !error = No errors
    if (!error) {
        NSDictionary *jsonDict = (NSDictionary*) [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        #ifndef DEBUGS
//        NSLog(@"%@", jsonDict);
        #endif
        currentSpotName = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"nomeluogo"]];
        idCurrentSpot = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"idluogo"]];
//        NSLog(@"idCurrentSpot ---> %@, %@", idCurrentSpot, currentSpotName);
        numberOfPost = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"numeronuovipost"]];
        NSArray *postList = [jsonDict objectForKey:@"listapost"];
        returnPostList = [[NSMutableArray alloc] initWithCapacity:postList.count];
        [postList enumerateObjectsUsingBlock:^(id obj,NSUInteger idx, BOOL *stop) {
            Post *post = [[Post alloc] initWithID:[[obj objectForKey:@"IDPOST"] integerValue]
                                         userName:[obj objectForKey:@"NOME"]
                                       userSurame:[obj objectForKey:@"COGNOME"]
                                   facebookUserID:[obj objectForKey:@"IDUTENTE"]
                                             body:[obj objectForKey:@"CORPO"]
                                       numberLike:[obj objectForKey:@"NUMMIPIACE"]
                                    numberComment:[obj objectForKey:@"NUMCOMMENTI"]
                                        timeStamp:[obj objectForKey:@"DATACREAZIONE"]
                                     userPhotoURL:[obj objectForKey:@"IMMAGINEUTENTE"]
                                     postPhotoURL:[obj objectForKey:@"IMMAGINEPOST"]
                                     iLikeEnabled:[obj objectForKey:@"IDPOSTMIPIACE"]];
            [returnPostList addObject:post];
        }];
        #ifndef DEBUGS
        NSLog(@"returnPostList.count: %d", returnPostList.count);
        #endif
        dictionaryToReturn = [[NSDictionary alloc] initWithObjectsAndKeys:currentSpotName, @"NomeLuogo", idCurrentSpot, @"idLuogo", numberOfPost, @"NumeroNuoviPost", returnPostList, @"PostList", nil];
    }
    
    return dictionaryToReturn;
}

@end

