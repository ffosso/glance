//
//  ViewController.h
//  Glance
//
//  Created by FFabio on 01/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    
    @private
    DataCenter *dataCenter;
    NSTimer *tryConnectionTimer;
    BOOL uialertAppear;
    UIAlertView *alertView;
    
}

@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(strong, nonatomic) IBOutlet UILabel *connectionErrorLabel;
@property(strong, nonatomic) IBOutlet UIImageView *alertImage;

@end
