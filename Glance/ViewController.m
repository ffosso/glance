//
//  ViewController.m
//  Glance
//
//  Created by FFabio on 01/03/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize activityIndicator = _activityIndicator, connectionErrorLabel = _connectionErrorLabel, alertImage = _alertImage;

- (void) viewDidLoad
{
    [super viewDidLoad];
    dataCenter = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getDataCenter];
	// Do any additional setup after loading the view, typically from a nib.
    self.activityIndicator.hidesWhenStopped = YES;
    [self.activityIndicator startAnimating];
    uialertAppear = FALSE;
    alertView = [[UIAlertView alloc]
                 initWithTitle:@"Errore"
                 message:@"Si è verificato un problema di connessione"
                 delegate:nil
                 cancelButtonTitle:@"Chiudi"
                 otherButtonTitles:nil];
    
    tryConnectionTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startApplication) userInfo:nil repeats:TRUE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Inner Methods

- (void) startApplication
{
    NSString *latitudeString = [NSString stringWithFormat:@"%.6f", ((CLLocation *) [dataCenter locations].lastObject).coordinate.latitude];
    NSString *longitudeString = [NSString stringWithFormat:@"%.6f", ((CLLocation *) [dataCenter locations].lastObject).coordinate.longitude];
    
//    WebApiManager *waManager = [[WebApiManager alloc] init];
    WebApiManager *waManager = [(AppDelegate *) [[UIApplication sharedApplication] delegate] getWebApiManager];
    
    dataCenter.spotListTipology = [waManager getSpotTipologyList];
    dataCenter.spotList = [waManager getSpotList];
    dataCenter.postList = [[waManager getPostByGPSWithLatitude:latitudeString andLongitude:longitudeString] objectForKey:@"PostList"];
    
    if (dataCenter.spotListTipology && dataCenter.spotList) {
        [alertView dismissWithClickedButtonIndex:0 animated:TRUE];
        uialertAppear = FALSE;
        [tryConnectionTimer invalidate];
        tryConnectionTimer = Nil;
        self.alertImage.hidden = TRUE;
        self.connectionErrorLabel.hidden = TRUE;
        [self.activityIndicator stopAnimating];
        [self performSegueWithIdentifier:@"startApp" sender:self];
    }
    else {
        if (!uialertAppear) {
            uialertAppear = TRUE;
            self.connectionErrorLabel.hidden = FALSE;
            self.alertImage.hidden = FALSE;
            [alertView show];
        }
    }
}

@end
