//
//  PostDetails_VC.h
//  Glance
//
//  Created by FFabio on 10/04/13.
//  Copyright (c) 2013 Blue Lion Software. All rights reserved.
//

#define FIRSTCELL 0

#import <UIKit/UIKit.h>
#import "Comment.h"
#import "PostCellWithPhotoCell.h"
#import "PostCell.h"
#import "CommentCell.h"

@interface PostDetails_VC : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate> {
    
    @private
    BOOL displayKeyboard;
    UITextField *activeUITextField;
    int activeUITextFieldOriginY;
    
    DataCenter *dataCenter;
    WebApiManager *waManager;
    Post *post;
    Spot *spot;
    NSCache *cachePostPhotos;
    NSCache *cachePostUserPhotos;
    
}

@property(strong, nonatomic) IBOutlet UILabel *spotName;
@property(strong, nonatomic) IBOutlet UITableView *mainTable;
@property(strong, nonatomic) IBOutlet UITextField *commentTextField;
@property(strong, nonatomic) IBOutlet UIButton *sendComment;

- (IBAction) sendILike:(id)sender;
- (void) setPostDetails:(Post *)postDetails andSpot:(Spot *)spotDetails;
- (void) openKeyboard:(BOOL)open;

@end
